Rails.application.routes.draw do
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'

  resources :sessions, only: [:create, :destroy]
  
  get 'sessions/create'

  get 'sessions/destroy'

  get 'character/index'

  resources :parties
  resources :characters

  get 'user/create'
  
  get 'welcome/index'
  
  get 'welcome/signup'
  
  get 'welcome/profile'
  
  root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
