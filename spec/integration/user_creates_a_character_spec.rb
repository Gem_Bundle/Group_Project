require 'rails_helper.rb'

feature"User creates a character" do
    scenario "User successfully navigates to the new character page from the listing characters page" do
       visit characters_path
       expect(page).to have_content("Listing characters")
       click_link "New character"
       expect(page).to have_content("New Character")
       expect(page).to have_field("Name")
       expect(page).to have_field("Race")
       expect(page).to have_field("character_klass")
       expect(page).to have_field("character_vitality")
       expect(page).to have_field("character_strength")
       expect(page).to have_field("character_agility")
       expect(page).to have_field("character_arcane")
       expect(page).to have_field("character_resistance")
       expect(page).to have_field("character_stamina")
    end
    
    scenario "User successfully creates a new character" do
        visit new_character_path
        expect(page).to have_content("New Character")
        fill_in "Name", with: 'Test Name'
        select('Pixie', :from =>'character_race')
        select('Healer', :from =>'character_klass')
        fill_in "character_vitality", with: 4
        fill_in "character_strength", with: 4 
        fill_in "character_agility", with: 4 
        fill_in "character_arcane", with: 4 
        fill_in "character_resistance", with: 4 
        fill_in "character_stamina", with: 4     
        click_button "Create Character"
        expect(page).to have_content("Test Name")
        expect(page).to have_content("Pixie")
        expect(page).to have_content("Healer")
        expect(page).to have_content(4)
    end
    
    scenario "User successfully edit a character" do
        visit new_character_path
        expect(page).to have_content("New Character")
        fill_in "Name", with: 'Test Name'
        select('Pixie', :from =>'character_race')
        select('Healer', :from =>'character_klass')
        fill_in "character_vitality", with: 4
        fill_in "character_strength", with: 4 
        fill_in "character_agility", with: 4 
        fill_in "character_arcane", with: 4 
        fill_in "character_resistance", with: 4 
        fill_in "character_stamina", with: 4     
        click_button "Create Character"
        expect(page).to have_content("Test Name")
        expect(page).to have_content("Pixie")
        expect(page).to have_content("Healer")
        expect(page).to have_content(4)
        
        visit characters_path
       expect(page).to have_content("Listing characters")
        click_link "Edit"
        fill_in "Name", with: 'Test Name_edited'
        select('Human', :from =>'character_race')
        select('Ranger', :from =>'character_klass')
        fill_in "character_vitality", with: 2
        fill_in "character_strength", with: 2 
        fill_in "character_agility", with: 2 
        fill_in "character_arcane", with: 2 
        fill_in "character_resistance", with: 2 
        fill_in "character_stamina", with: 2     
        click_button "Update Character"
        expect(page).to have_content("Test Name_edited")
        expect(page).to have_content("Human")
        expect(page).to have_content("Ranger")
        expect(page).to have_content(2)
        end
        
        scenario "User successfully deletes a character" do
        visit new_character_path
        expect(page).to have_content("New Character")
        fill_in "Name", with: 'Test Name'
        select('Pixie', :from =>'character_race')
        select('Healer', :from =>'character_klass')
        fill_in "character_vitality", with: 4
        fill_in "character_strength", with: 4 
        fill_in "character_agility", with: 4 
        fill_in "character_arcane", with: 4 
        fill_in "character_resistance", with: 4 
        fill_in "character_stamina", with: 4     
        click_button "Create Character"
        expect(page).to have_content("Test Name")
        expect(page).to have_content("Pixie")
        expect(page).to have_content("Healer")
        expect(page).to have_content(4)
        
        visit characters_path
        expect(page).to have_content("Listing characters")
        click_link "Destroy"
        expect(page).to  have_no_content("Test Name")
        expect(page).to  have_no_content("Pixie")
        expect(page).to  have_no_content("Healer")
        expect(page).to  have_no_content(4)
       end
       
    end