class AddPartyIdToCharacters < ActiveRecord::Migration[5.1]
  def change
    create_table :characters_parties, id: false do |t|
      t.belongs_to :party, index: true
      t.belongs_to :character, index: true
    end
  end
end
