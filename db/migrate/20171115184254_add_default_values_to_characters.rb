class AddDefaultValuesToCharacters < ActiveRecord::Migration[5.1]
  def up
    change_column :characters, :vitality, :integer, default: 1
    change_column :characters, :strength, :integer, default: 1
    change_column :characters, :agility, :integer, default: 1
    change_column :characters, :arcane, :integer, default: 1
    change_column :characters, :resistance, :integer, default: 1
    change_column :characters, :stamina, :integer, default: 1
  end
  
  def down
    change_column :characters, :vitality, :integer, default: nil
    change_column :characters, :strength, :integer, default: nil
    change_column :characters, :agility, :integer, default: nil
    change_column :characters, :arcane, :integer, default: nil
    change_column :characters, :resistance, :integer, default: nil
    change_column :characters, :stamina, :integer, default: nil
  end
end
