class RemoveStatsFromCharacters < ActiveRecord::Migration[5.1]
  def change
    remove_column :characters, :Stats, :string
    add_column :characters, :vitality, :integer
    add_column :characters, :strength, :integer
    add_column :characters, :agility, :integer
    add_column :characters, :arcane, :integer
    add_column :characters, :resistance, :integer
    add_column :characters, :stamina, :integer
  end
end
