class CreateMemberships < ActiveRecord::Migration[5.1]
  def up
    create_table :memberships do |t|
      t.integer :party_id
      t.integer :character_id

      t.timestamps
    end
    
    drop_table :characters_parties
  end
end
