# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171206211749) do

  create_table "characters", force: :cascade do |t|
    t.string "name"
    t.string "race"
    t.string "klass"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "stats"
    t.integer "vitality", default: 1
    t.integer "strength", default: 1
    t.integer "agility", default: 1
    t.integer "arcane", default: 1
    t.integer "resistance", default: 1
    t.integer "stamina", default: 1
    t.integer "user_id"
    t.index ["user_id"], name: "index_characters_on_user_id"
  end

  create_table "memberships", force: :cascade do |t|
    t.integer "party_id"
    t.integer "character_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "parties", force: :cascade do |t|
    t.string "name"
    t.integer "size"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["user_id"], name: "index_parties_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "provider"
    t.string "uid"
    t.string "name"
    t.string "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
