Feature: Create a new character
  
  As a user
  So that I can use the website 
  I want to be able to create a new character
  
Scenario: As a user I want to be able to navigate from the characters page to the new character page
  Given I am on the characters page
  When I click on the "New character" link
  Then I should be on the "New Character" page
  And I should see the "Name" field
  And I should see the "Race" field
  And I should see the "character_klass" field
  And I should see the "character_vitality" field
  And I should see the "character_strength" field
  And I should see the "character_agility" field
  And I should see the "character_arcane" field
  And I should see the "character_resistance" field
  And I should see the "character_stamina" field
  And I should see the "Create Character" button