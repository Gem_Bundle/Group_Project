#OLD USER CONTROLLER, FUNCTIONS HERE ARE NOT USED
class UserController < ApplicationController
  # GET /users/1
  def show
    @user = User.find(params[:id])
  end
  # GET /users/new
  def new
    @user = User.new
  end
   # POST /users
  def create
    @user = User.new(user_params())
    
    if @user.save
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def index
  end
end

private
  def user_params
    params.require(:user).permit(:uname, :psw, :chalist)
  end