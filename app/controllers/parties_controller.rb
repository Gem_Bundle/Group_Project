class PartiesController < ApplicationController
    #Access current_user in model


    def create
        @user = current_user    #Define current user
        @party = @user.parties.create(party_params)   #Create a new party belonging to the current user
        @characters = Character.where(:id => params[:characters_ids])  #Find characters based on their character ID's
        @party.characters << @characters    #Add those characters to the party's list of characters
        
        if @party.save
          redirect_to @party
        else
          render 'new'
        end
    end
    
    def index
        @parties = Party.all
    end
    
    def new
        @user = current_user
        @party = Party.new
    end
    
    def show
        @party = Party.find(params[:id])
        @characters = Character.includes(:parties).where(parties: {id: @party.id})   #Finds alls of the characters in the party 
    end
    
    def edit
        @party = Party.find(params[:id])
        @user = current_user
    end
    
    def update
        @party = Party.find(params[:id])
        if @party.update(party_params)    #Finds and updates the party object
            redirect_to @party
        else
            render 'edit'
        end
    end
    
    def destroy
        @user = current_user
        @party = @user.parties.find(params[:id])   #Find the party among the parties the user has
        @party.destroy
        
        redirect_to parties_path
    end
end

private
    def party_params                #Define party parameters, accepts an array of character ID's to assign to the party
        params.require(:party).permit(:name, character_ids:[])
    end