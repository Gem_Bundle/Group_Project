class SessionsController < ApplicationController
#Create a sessions 
  def create
    user = User.from_omniauth(request.env["omniauth.auth"]) #Gets Google authentication
    session[:user_id] = user.id   #Creates a session for the user
    redirect_to root_path
  end
# destroy session
  def destroy
    session[:user_id] = nil   #Ends the session on sign out
    redirect_to root_path
  end
end
