class CharactersController < ApplicationController
  # GET /characters
  def index
    @characters = Character.all  #Fetch all characters
  end
  
  def show
    @character = Character.find(params[:id]) #Find character based on character attributes
  end
  
  # GET /characters/new
  def new
    @character = Character.new
  end
  # POST /characters
  def create
    @user = current_user   #Define current user
    @character = @user.characters.create(character_params)   #Create a new character belonging to the current user
    
    if @character.save
      redirect_to @character  #redirect to show character
    else
      render 'new'
    end
  end
  
  # GET /characters/1/edit
  def edit
    @character = Character.find(params[:id])   #Find character based on character attributes
  end
    
  def update
    @character = Character.find(params[:id])   #Find character based on character attributes
    if @character.update(character_params)
      redirect_to @character
    else
      render 'edit'
    end
  end
   # DELETE /characters/1
  def destroy
    @user = current_user   #Define current user
    @character = @user.characters.find(params[:id])   #Find the character that belongs to the current user with the given parameters
    @character.destroy    #Remove the character
    
    redirect_to characters_path
  end
end

private
  def character_params                #Character parameters that are inputted by the user
    params.require(:character).permit(:name, :race, :klass, :vitality, :strength, :agility, :arcane, :resistance, :stamina)
  end