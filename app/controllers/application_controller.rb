# Prevent attacks by raising an exception.
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user #Defines helper method to return the user currenting in the app

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id] #Fetches the user logged in now
  end
end
