class Membership < ApplicationRecord
    belongs_to :party       #Owned by parties and characters, defines the relationship between them
    belongs_to :character
end
