class Character < ApplicationRecord
    has_many :memberships       #Has many memberships that define the relationship between characters and parties
    has_many :parties, through: :memberships
    belongs_to :user        #Owned by a user model
end
