class Party < ApplicationRecord
    has_many :memberships           #Has many memberships that define the associations with characters
    has_many :characters, through: :memberships 
    belongs_to :user        #Owned by the user model
end
